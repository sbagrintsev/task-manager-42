package ru.tsc.bagrintsev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "m_task")
@NoArgsConstructor
public final class Task extends AbstractWBSModel {

    @Nullable
    @ManyToOne
    private Project project;

}

package ru.tsc.bagrintsev.tm.api.model;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasDateCreated {

    @NotNull
    Date getDateCreated();

    void setDateCreated(@NotNull final Date dateCreated);

}

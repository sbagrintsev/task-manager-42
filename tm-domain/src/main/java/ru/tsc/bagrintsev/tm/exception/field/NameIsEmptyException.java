package ru.tsc.bagrintsev.tm.exception.field;

public final class NameIsEmptyException extends AbstractFieldException {

    public NameIsEmptyException() {
        super("Error! Name is empty...");
    }

}

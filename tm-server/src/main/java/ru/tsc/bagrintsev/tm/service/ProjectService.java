package ru.tsc.bagrintsev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<ProjectDTO, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @NotNull ProjectDTO add(
            @Nullable final String userId,
            @Nullable final ProjectDTO project
    ) throws ModelNotFoundException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (project == null) throw new ModelNotFoundException();
        project.setUserId(userId);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    public @NotNull ProjectDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, ProjectNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable ProjectDTO project;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            if (status.equals(Status.IN_PROGRESS)) {
                project.setStatus(status);
                project.setDateStarted(new Date());
            } else if (status.equals(Status.COMPLETED)) {
                project.setStatus(status);
                project.setDateFinished(new Date());
            } else if (status.equals(Status.NOT_STARTED)) {
                project.setStatus(status);
                project.setDateStarted(null);
                project.setDateFinished(null);
            }
            update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clearAll() {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clearAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @Override
    public @NotNull ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existsById(userId, id);
        }
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final List<ProjectDTO> list = repository.findAll();
            return (list == null) ? Collections.emptyList() : list;
        }
    }

    @Override
    public @NotNull List<ProjectDTO> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final List<ProjectDTO> list = repository.findAllByUserId(userId);
            return list == null ? Collections.emptyList() : list;
        }
    }

    @Override
    public @NotNull List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final String order = getQueryOrder(sort);
            @Nullable final List<ProjectDTO> list = repository.findAllSort(userId, order);
            return list == null ? Collections.emptyList() : list;
        }
    }

    @NotNull
    @Override
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable ProjectDTO project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @NotNull
    @Override
    public ProjectDTO removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            repository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    public @NotNull Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> projects) {
        if (projects.isEmpty()) return projects;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.addAll(projects);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return projects;
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.totalCountByUserId(userId);
        }
    }

    @Override
    public void update(@Nullable final ProjectDTO project) throws ModelNotFoundException {
        if (project == null) throw new ModelNotFoundException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @Nullable ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.updateById(userId, id, name, description);
            sqlSession.commit();
            project = repository.findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

}

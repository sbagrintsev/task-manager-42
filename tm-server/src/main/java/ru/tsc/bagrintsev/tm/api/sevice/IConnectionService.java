package ru.tsc.bagrintsev.tm.api.sevice;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull EntityManagerFactory getEntityManagerFactory();

    @NotNull SqlSession getSqlSession();

    @NotNull SqlSessionFactory getSqlSessionFactory();

}

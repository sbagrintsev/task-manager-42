package ru.tsc.bagrintsev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @Insert("INSERT INTO m_project " +
            "(id, date_created, name, description, status, user_id, date_started, date_finished) " +
            "VALUES " +
            "(#{id}, #{dateCreated}, #{name}, #{description}, #{status}, #{userId}, #{dateStarted}, #{dateFinished};")
    void add(@NotNull final ProjectDTO record);

    @Insert({
            "<script>",
            "INSERT INTO m_project ",
            "(id, date_created, name, description, status, user_id, date_started, date_finished) ",
            "VALUES" +
                    "<foreach item='item' collection='records' open='' separator=',' close=''>" +
                    "(" +
                    "#{item.id},",
            "#{item.dateCreated},",
            "#{item.name},",
            "#{item.description},",
            "#{item.status},",
            "#{item.userId},",
            "#{item.dateStarted},",
            "#{item.dateFinished}" +
                    ")" +
                    "</foreach>",
            "</script>"})
    void addAll(@Param("records") @NotNull final Collection<ProjectDTO> records);

    @Override
    @Delete("DELETE " +
            "FROM m_project " +
            "WHERE user_id = #{userId};")
    void clear(@Param("userId") @NotNull final String userId);

    @Override
    @Delete("DELETE " +
            "FROM m_project;")
    void clearAll();

    @Override
    @Select("SELECT count(*) = 1 " +
            "FROM m_project " +
            "WHERE user_id = #{userId} " +
            "AND id = #{id};")
    boolean existsById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id
    );

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, date_started, date_finished " +
            "FROM m_project;")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    List<ProjectDTO> findAll();

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, date_started, date_finished " +
            "FROM m_project " +
            "WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    List<ProjectDTO> findAllByUserId(@Param("userId") @NotNull final String userId);

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, date_started, date_finished " +
            "FROM m_project " +
            "WHERE user_id = #{userId} " +
            "ORDER BY #{order};")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    List<ProjectDTO> findAllSort(
            @Param("userId") @NotNull final String userId,
            @Param("order") @NotNull final String order
    );

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, date_started, date_finished " +
            "FROM m_project " +
            "WHERE user_id = #{userId} " +
            "AND id = #{id};")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    ProjectDTO findOneById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id
    );

    @Override
    @Delete("DELETE " +
            "FROM m_project " +
            "WHERE user_id = #{userId} " +
            "AND id = #{id};")
    void removeById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id
    );

    @Override
    @Select("SELECT count(*) " +
            "FROM m_project;")
    long totalCount();

    @Override
    @Select("SELECT count(*) " +
            "FROM m_project " +
            "WHERE user_id = #{userId};")
    long totalCountByUserId(@Param("userId") @NotNull final String userId);

    @Override
    @Update("UPDATE m_project " +
            "SET name = #{name}, " +
            "description = #{description}, " +
            "status = #{status}, " +
            "user_id = #{userid}, " +
            "date_started = #{dateStarted}, " +
            "date_finished = #{dateFinished} " +
            "WHERE id = #{id};")
    void update(@NotNull final ProjectDTO project);

    @Update("UPDATE m_project " +
            "SET name = #{name}, " +
            "description = #{description} " +
            "WHERE user_id = #{userid}" +
            "AND id = #{id};")
    void updateById(
            @Param("userId") @Nullable final String userId,
            @Param("id") @Nullable final String id,
            @Param("name") @Nullable final String name,
            @Param("description") @Nullable final String description
    );

}
